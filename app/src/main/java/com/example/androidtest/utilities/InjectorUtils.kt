package com.example.androidtest.utilities

import com.example.androidtest.api.LoginService
import com.example.androidtest.data.LoginRepository
import com.example.androidtest.viewmodels.MainViewModelFactory

object InjectorUtils {
    fun provideMainViewModelFactory(): MainViewModelFactory {
            val repository = LoginRepository(LoginService.create())
        return MainViewModelFactory(repository)
    }
}