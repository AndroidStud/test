package com.example.androidtest.utilities

import android.content.Context
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.FragmentActivity

class LoginUtils {
    companion object {
        // Check otp validation for non empty
        fun otpValidation(otp: String): Boolean {
            return otp.isNotEmpty()
        }

        /*
         * Hides the soft keyboard
        */
        fun hideSoftKeyboard(activity: FragmentActivity?) {
            if (activity?.currentFocus != null) {
                val inputMethodManager =
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(
                    activity.currentFocus!!.windowToken,
                    0
                )
            }
        }
    }
}