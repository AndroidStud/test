package com.example.androidtest

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 * As per the requirement, MainActivity is the main class. It shows MainFragment
 * and then on submit(icon), it moves to SecondFragment.
 * On back press from second fragment, first fragment is shown.
 */
class MainActivity : AppCompatActivity(), MainFragment.CallSecondFragment {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.container, MainFragment.newInstance())
                .commit()
        }
    }

    override fun showSecondFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, SecondFragment.newInstance())
            .addToBackStack(null)
            .commit()
    }

    override fun onBackPressed() {
        if (supportFragmentManager?.backStackEntryCount != 0) {
            supportFragmentManager?.popBackStack()
        } else {
            super.onBackPressed()
        }
    }
}