package com.example.androidtest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.androidtest.databinding.SecondFragmentBinding

/**
 * As per requirement this class comes after first fragment.
 * It displays lottie animation implemented in xml and handle toolbar
 * back button click
 */
class SecondFragment : Fragment() {

    companion object {
        fun newInstance() = SecondFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val secondFragmentBinding = DataBindingUtil.inflate<SecondFragmentBinding>(
            inflater, R.layout.second_fragment, container, false
        )

        secondFragmentBinding.toolbar.setOnClickListener {
            if (activity?.supportFragmentManager?.backStackEntryCount != 0) {
                activity?.supportFragmentManager?.popBackStack()
            }
        }

        return secondFragmentBinding.root
    }
}