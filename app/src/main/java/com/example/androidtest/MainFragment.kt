package com.example.androidtest

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.observe
import com.example.androidtest.data.LoginOtp
import com.example.androidtest.databinding.MainFragmentBinding
import com.example.androidtest.utilities.InjectorUtils
import com.example.androidtest.utilities.LoginUtils
import com.example.androidtest.viewmodels.MainViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.main_fragment.*

/**
 * This class the first screen shown to user. It uses two interfaces
 * 1. Callback -> This binds submit click to this class.
 * 2. CallSecondFragment -> When user successfully enters the otp,
 * it request the Main Activity to show second fragment.
 */
class MainFragment : Fragment() {
    private var listener: CallSecondFragment? = null
    private var mainFragmentBinding: MainFragmentBinding? = null

    private val mainViewModel: MainViewModel by viewModels {
        InjectorUtils.provideMainViewModelFactory()
    }

    companion object {
        fun newInstance() = MainFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as CallSecondFragment
        } catch (castException: ClassCastException) {
            /** The activity does not implement the listener.  */
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mainFragmentBinding = MainFragmentBinding.inflate(
            inflater, container, false
        ).apply {
            viewModel = mainViewModel
            lifecycleOwner = viewLifecycleOwner
            callback = object : Callback {
                override fun sendOtpRequest() {
                    if (LoginUtils.otpValidation(mainFragmentBinding?.etOtp?.text.toString())) {
                        progressBar.visibility = View.VISIBLE
                        LoginUtils.hideSoftKeyboard(activity)
                        mainViewModel.postOtp(LoginOtp(mainFragmentBinding?.etOtp?.text.toString()))
                    } else {
                        Snackbar.make(root, R.string.login_otp_empty_error, Snackbar.LENGTH_LONG)
                            .show()
                    }
                }
            }
        }

        // Keyboard Action button (Submit) has been linked to hiting otp api.
        mainFragmentBinding!!.etOtp.setOnEditorActionListener { _, id, _ ->
            if (id == EditorInfo.IME_ACTION_DONE) {
                mainFragmentBinding!!.ivSubmit.performClick()
                true
            } else false
        }

        // isSuccess gets its value after login api returns response.
        // True -> Move to second fragment.
        // False -> Show snackbar.
        mainViewModel.isSuccess?.observe(viewLifecycleOwner) {
            progressBar.visibility = View.GONE
            if (!it) {
                Snackbar.make(
                    mainFragmentBinding!!.root,
                    R.string.login_otp_invalid_error,
                    Snackbar.LENGTH_LONG
                )
                    .show()
            } else {
                mainViewModel.isSuccess = MutableLiveData()
                listener?.showSecondFragment()
            }
        }

        return mainFragmentBinding!!.root
    }

    interface Callback {
        fun sendOtpRequest()
    }

    interface CallSecondFragment {
        fun showSecondFragment()
    }
}
