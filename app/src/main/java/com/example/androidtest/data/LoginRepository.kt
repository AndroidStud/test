package com.example.androidtest.data

import com.example.androidtest.api.LoginService
import io.reactivex.Observable
import retrofit2.Response

class LoginRepository(private val service: LoginService) {
    fun postLoginOtpData(loginOtp: LoginOtp): Observable<Response<Void>> {
        return service.postLoginOtp(loginOtp)
    }
}