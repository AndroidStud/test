package com.example.androidtest.data

import com.google.gson.annotations.SerializedName

/**
 * Data class which takes otp as code and send it to api {"code:####"}
 */
data class LoginOtp(
    @field:SerializedName("code") val code: String
)