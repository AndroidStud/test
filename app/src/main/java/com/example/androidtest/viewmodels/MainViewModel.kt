package com.example.androidtest.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.androidtest.data.LoginOtp
import com.example.androidtest.data.LoginRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class MainViewModel internal constructor(
    private val loginRepository: LoginRepository
) : ViewModel() {
    private val disposables = CompositeDisposable()

    //On any change to isSuccess, it's observer would be called.
    var isSuccess: MutableLiveData<Boolean>? = null

    init {
        isSuccess = MutableLiveData()
    }

    fun postOtp(loginOtp: LoginOtp) {
        disposables.add(
            loginRepository.postLoginOtpData(loginOtp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<Response<Void>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(response: Response<Void>) {
                        isSuccess?.value = response.isSuccessful
                    }

                    override fun onError(e: Throwable) {
                        Log.d("message", e.message.toString())
                    }
                })
        )
    }
}
