package com.example.androidtest

import com.example.androidtest.utilities.LoginUtils
import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.junit.Test

class LoginUtilsTest {
    @Test
    fun testOTPValid_ReturnsTrue() {
        assertTrue(LoginUtils.otpValidation("1234"));
    }

    @Test
    fun testOTPEmpty_ReturnsFalse() {
        assertFalse(LoginUtils.otpValidation(""));
    }
}