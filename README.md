# README #

### What is this repository for? ###
This repository is an android test code. It features various things like      
* Android new components (JetPack)  
* Login screen  
* Api implementation using retrofit  
* Animation using Lottie   
* Material design for toolbar   
* Handling small things for ease of application like Keyboard handling,
Orientation of screen, Back button handling, Progress dialog wherever necessary.   
* New layout structure using Constraint layouts.   
* Android data injection library   
* JUnit test cases   
* MVVM architecture   


### How do I get set up? ###
Simply clone the repository using the button above.

* Specification   
Name of the application - AndroidTest   
Compile Sdk Version - 30   
Minimum Sdk Version - 16   
Version Name - 1.0   